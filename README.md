# FDMR Bridge

<img src="https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-Docker/main/Easy-FreeDMR-Docker.png" width="550" height="450"><img src="https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/main/screenshot.png" width="550" height="450">

Is an excerpt from the emq-TE1ws proyect, focused on new and current sysops who want to install FreeDMR easily, quickly and up-to-date.

This shell, install FreeDMR Server,with 2 option Dashboard for select: FDMR-Monitor by OA4DOA Christian and FDMR-Monitor by CS8ABG Bruno , Both Dashboard version Self-Service

#

Shell FDMR Bridge , FreeDMR Bridge mode with rules, without Docker, latest original gitlab hacknix version by G7RZU Simon, with Dashboard, template mods by WP3JM James & N6DOZ Rudy, Self-Service mods with Dial-TG by IU2NAF Diego and menu by HP3ICC.

# Important note , 

* Please note that this is the Bridge version and not the Server version

* Compatibility

You can use this script on raspberry , linux pc , server , virtual machine or vps with debian 11 x86 or x64

This script contains binaries created by different developers , many designed to be used on debian 11 or higher , bad news for ubuntu users , some of the included applications may only work correctly on debian 11

* Very important, if you are going to use this installation script to integrate your server into the freedmr mesh, you must first have the basic knowledge necessary to administer your server, and you must comply with the configurations required to be part of the network.

 Below are the strict compliance configurations.

 [GLOBAL]

VALIDATE_SERVER_IDS: True


[ALIASES]

TRY_DOWNLOAD: True

SERVER_ID_URL: https://freedmr-lh.gb7fr.org.uk/json/server_ids.tsv


[SYSTEM]

ALLOW_UNREG_ID: False

PROXY_CONTROL: True


[OBP-TEST]

TGID_ACL: DENY:0-82,92-199,800-899,9990-9999,900999


The TGID_ACL line should be applied to all your obp if your server is part of the freedmr server mesh


* Support

Unofficial script to install Freedmr Server with Dashboard self-service, if you require support from the official version of the developer , refer to the original developer script :

https://gitlab.hacknix.net/hacknix/FreeDMR/-/wikis/Installing-using-Docker-(recommended!)

FreeDMR Server original version gitlab FreeDMR by G7RZU hacknix Simon.

#

# Pre-Requirements

need have curl and sudo installed

#

# Install

into your ssh terminal copy and paste the following link :

    apt-get update

    apt-get install curl sudo -y

    sudo su
    
    bash -c "$(curl -fsSL https://gitlab.com/hp3icc/fdmr-b/-/raw/main/install.sh)"
             
             
 #            
  
 # Menu
 
 ![alt text](https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-SERVER-Install/main/IMG_1941.jpg)
 
  At the end of the installation your freedmr server will be installed and working, a menu will be displayed that will make it easier for you to edit, restart or update your server and dashboard to future versions.
  
  to use the options menu, just type "menu-fdmr" without the quotes in your ssh terminal or console.
  
 #
 
 # Location files config :
 
  * FreeDMR Server:  
   
   /opt/FreeDMR-B/config/FreeDMR.cfg
   
  * FreeDMR Rules: 
   
   /opt/FreeDMR-B/config/rules.py
   
  * FDMR-Monitor: 
   
   /opt/FDMR-Monitor-B/fdmr-mon.cfg 

    * FDMR-Monitor2: 
   
   /opt/FDMR-Monitor-B2/fdmr-mon.cfg 
   
  #
  
  # Systemctl Services :
  
  * Freedmr Bridge: 
   
   freedmr-bridge.service
   
  * FreeDMR Proxy: 
   
   proxy2.service
   
  * FDMR-Monitor-B: 
   
   fdmr_mon-b.service
   
  * Web Server
  
   http.server-fdmr-b.service

  * FDMR-Monitor-B2: 
   
   fdmr_mon-b2.service
   
  * Web Server 2
  
   http.server-fdmr-b2.service
  
 #
  
 # Dashboard Files
 
 * FDMR-Monitor by OA4DOA

 /var/www/fdmr-b/

* FDMR-Monitor2 by CS8ABG

 /var/www/fdmr-b2/

#

 # Credits :
 
Special thanks to colleagues: CA5RPY Rodrigo, CS8ABG Bruno, OA4DOA Christian, G7RZU hacknix, for their contributions to the content of this scrip.

#

 # Sources :
 
 * https://gitlab.hacknix.net/hacknix/FreeDMR
 
 * http://www.freedmr.uk/index.php/freedmr-server-install/

 * https://github.com/CS8ABG/FDMR-Monitor/tree/Self_Service
 
 * https://github.com/yuvelq/FDMR-Monitor/tree/Self_Service
 

